﻿#include <iostream>
using namespace std;

class Player
{
public:
	char name[256] = "имя";
	int score = 0;

	void EnterData()
	{
		cin >> name >> score;
	}
	void ShowData()
	{
		cout << name << "\t" << score << endl;
	}
};

int main()
{
	setlocale(LC_ALL,"ru");

	int size = 0;
	cout << "введите количество игроков: ";
	cin >> size;
	Player* Array = new Player[size];

	for (int i = 0; i < size; ++i)
	{
		cout << "Введите " << i + 1 << " имя игрока и счёт через TAB:";
		Array[i].EnterData();
	}
	cout << endl;
	
	for (int i = 0; i < size; i++)
	{
		for (int j = size - 1; j > i; j--)
		{
			if ((Array[j].score) > (Array[j - 1].score))
			{
				swap(Array[j], Array[j - 1]);
			}
		}
	}
	cout << endl;
	cout << "Отсортированный список:" << endl;

	for (int i = 0; i < size; ++i)
	{
		Array[i].ShowData();
	}
	delete[]Array;
}
